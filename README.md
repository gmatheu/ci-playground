# README #

 Containerized CI tools bundle. 

### What is this repository for? ###

The idea is to have the tool with full administration permission in order play and test new configurations before messing up the real environments.

### How do I get set up? ###

* Start
`docker-compose up`

* Dependencies
** Docker
** Docker-compose
