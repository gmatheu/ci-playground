#! /bin/bash
sudo yum install -y java-1.7.0-openjdk subversion vim git mysql mysql-server

sudo yum install -y ntpdate
sudo ntpdate pool.ntp.org

sudo wget -O /etc/yum.repos.d/jenkins.repo \ 
  http://pkg.jenkins-ci.org/redhat/jenkins.repo
sudo rpm --import http://pkg.jenkins-ci.org/redhat/jenkins-ci.org.key
sudo yum install -y jenkins-1.598-1.1

sudo wget -O /etc/yum.repos.d/bintray-jfrog-artifactory-rpms.repo https://bintray.com/jfrog/artifactory-rpms/rpm 
sudo yum install -y artifactory

sudo wget -O /etc/yum.repos.d/sonar.repo http://downloads.sourceforge.net/project/sonar-pkg/rpm/sonar.repo
sudo yum install -y sonar

# certificates

# jobs

# cp ~/.m2/settings.xml /var/lib/jenkins
# import ssl certificate
sudo service jenkins restart
sudo service mysqld restart
sudo service sonar restart
sudo service artifactory restart

mysql -u root -p < `curl -s https://raw.githubusercontent.com/SonarSource/sonar-examples/master/scripts/database/mysql/create_database.sql` 
